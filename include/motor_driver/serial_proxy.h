#include <stdint.h>
#include <stdlib.h>


#ifndef SERIAL_COM_H
#define SERIAL_COM_H


#define 		PING 				0x01
#define			GOAL_POSITION 		0x02
#define			SET_VEL				0x03
#define			GOAL_VEL_PROFILE	0x04
#define			DISABLE_TX			0x05
#define			ENABLE_TX			0x06

#define			FEEDBACK3 			0x11

#define			PGAIN				0x21
#define			IGAIN				0x22
#define			DGAIN				0x23
#define			DUTY 				0x24

#define			GET_VEL 			0x25
#define			GET_POS 			0x26


#define			TX_LENGTH			0x06





class MotorIO {

private:
	const char* serialDevName;
	char const *portName;
	
	int fd;
	bool rx_enabled, rx_complete;

	/* UART buffers and variables*/
	unsigned char bufferRx[32];
	unsigned char bufferTx[32];

	/* CRC variables*/
	unsigned short crctable16[256];
	void generate_CRC16();
	uint8_t _crcTx(uint8_t *, uint8_t);
	bool _crcRx(uint8_t *buf);

	bool read_response(int16_t);
public:
	const char *devFTDI;
	const char *dev1;
	const char *dev2;
	int16_t position; /**< the position that the motor is ending back */
	int16_t *p;		/**< The pointer that 'point' to the position member */
	//
	int16_t velocity;	/**< The velocity feedback from the motor */
 	int16_t *v;			/**< The pointer to the 'velocity' */
	//
	int16_t current;	/**< The current feedbak from the motor */
	int16_t *c;			/**< The pointer to the 'currenet' */
	uint8_t dummy;
	//
	uint8_t motors_left[6], motors_right[6];
	// Constructor
	/**
	 * \brief constructor
	 *
	 * This constructor uses the default serial port "/dev/ttyUSB0" which correspond
	 * to the FTDI board.
	 */
	MotorIO();
	/**
	 * \brief Constructor
	 *
	 * This constructor takes as arguments the:
	 * 
	 * -device: Either '/dev/ttyS0' (UART molex connector) or
	 * '/dev/ttyS2' (pins #8 and #10)
	 * 
	 * 
	 * -name: Gives the opprtunity to name each port (e.g. left/right leg)
	 * to be used mainly on logging information.
	 */
	MotorIO(const char *, char const *);

	/*! \brief Opens the serial port defined by the constructor and
     * generates the CRC16 lookup table
     */
	void start();
	/*! \brief Closes the serial port
    */
	void stop();
	/*! \brief Pings for a motor
	 * 
	 * Arguments: the slave adrress to look if it connected to the network.
	 *
	 *
	 * Returns the address of the pinged slave OR 0 zero if no response to ping
	 */
	uint8_t ping(uint8_t);
	//
	/*! \brief Sets the goal position of the motor with the specified address.
	 */
	uint8_t goal_position(uint8_t, float);
	/*! \brief Sets the goal velocity of the motor with the specified address.
	 */
	uint8_t goal_velocity(uint8_t, int16_t);
	/*! \brief sets the gain of a PID controller of the motor with the specified address.
	 */
	uint8_t set_gain(uint8_t, uint8_t, float);
	//
	uint8_t feedback3(uint8_t, float &, float &, float &);
	uint8_t update_motors_list(uint8_t *);
	uint8_t setDuty(uint8_t, uint8_t);
	uint8_t get_position(uint8_t, int16_t &);
	uint8_t get_velocity(uint8_t, int16_t &);



};

#endif