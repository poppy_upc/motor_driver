#include <stdio.h>   	/* Standard input/output definitions */
#include <string.h>  	/* String function definitions */
#include <unistd.h>  	/* UNIX standard function definitions */
#include <fcntl.h>   	/* File control definitions */
#include <errno.h>   	/* Error number definitions */
#include <termios.h>	/* POSIX terminal control definitions */
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdint.h>
#include <stdarg.h>
#include <iostream>
#include <stdlib.h>
#include <typeinfo>
#include <time.h>
#include <ros/ros.h>
#include <ros/package.h>
#include <ros/console.h>
#include <sstream>

#include "motor_driver/serial_proxy.h"


/* ~~ Constructor ~~ */
MotorIO::MotorIO() {

	serialDevName = "/dev/ttyUSB0";	// Default port (FTDI programmer).
    portName = serialDevName;

}

MotorIO::MotorIO(const char * device, char const *name) {
    serialDevName = device;
    /* Odroid C1 exposed UART pins
            "dev/ttyS2" -> #8 and #10
            "dev/ttyS0" -> UART molex connector
    */
    portName = name;
}


void MotorIO::start() {
	struct termios port_options;
	int16_t status;
	speed_t baud = B500000;

	fd = open(serialDevName, O_RDWR | O_NOCTTY | O_NDELAY);
	if (fd == -1) {
		ROS_INFO("!!Couldn't open port '%s'!", serialDevName);
	}else {
		ROS_INFO("***Device '%s' opened succesfully.", serialDevName);
	}

	// fcntl(fd, F_SETFL, O_RDWR);
    fcntl(fd, F_SETFL, 0);

	// Get the current options of the port
	tcgetattr(fd, &port_options);
	/* Set the baud rate */
	cfmakeraw(&port_options);
	cfsetispeed(&port_options, baud);
	cfsetospeed(&port_options, baud);
	// Enable the receiver and set local mode
	port_options.c_cflag |= (CLOCAL | CREAD);
	/* Setting the Parity Checking */
	// No parity (8N1: 8bits, No parity, 1 stop bit)
	port_options.c_cflag &= ~PARENB;
	port_options.c_cflag &= ~CSTOPB;
	port_options.c_cflag &= ~CSIZE;
	port_options.c_cflag |= CS8;
	/* set the character size */
	 /* Mask the character size bits */
	/* Setting Hardware Flow Control */
	// To disable hardware flow control (CTS, RTS):
	port_options.c_cflag &= ~CRTSCTS;
	/* Choosing Raw Input */
	// Raw input is unprocessed. Input characters are passed through exactly as they
	// are received, when they are received.
	port_options.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG);
	/* Choosing Raw Output */
	// Raw output is selected by resetting the OPOST option in the c_oflag member:
	port_options.c_oflag &= ~OPOST;
	port_options.c_iflag &= ~(IXON | IXOFF | IXANY);
	/* Setting Read Timeouts */
	// VMIN specifies the minimum number of characters to read. If it is set to 0,
	// then the VTIME value specifies the time to wait for every character read.
	// Note that this does not mean that a read call for N bytes will wait for N characters
	// to come in. Rather, the timeout will apply to the first character and the read call
	// will return the number of characters immediately available (up to the number you request).
	port_options.c_cc [VMIN]  = 0;	// Minimum number of characters for noncanonical read (MIN).
    port_options.c_cc [VTIME] = 1;		// Timeout in deciseconds for noncanonical read (TIME). 
    //								   1 second (10 deciseconds)
    /* Set the new options for the flag */
	tcsetattr (fd, TCSANOW | TCSAFLUSH, &port_options);

	ioctl (fd, TIOCMSET, &status);
  	status |= TIOCM_DTR ;
  	status |= TIOCM_RTS ;
  	ioctl (fd, TIOCMSET, &status);

  	usleep(10000);	// 10ms

	bufferTx[0] = 0xFF;
    bufferTx[1] = 0xFF;
    rx_enabled = false;
    rx_complete = false;
    // write(fd, bufferTx,2);
	// Generate the CRC16 lookup table
	generate_CRC16();
    dummy = 0x00;
}


void MotorIO::stop() {

	close(fd);
	ROS_INFO("*** Device '%s' closed succesfully.\r\n", serialDevName);

}

bool MotorIO::read_response(int16_t num) {
    int16_t rcv = read(fd, bufferRx, num);
    usleep(250);    // Don't ask :S
    if (rcv != num) {
        uint8_t *pnt;
        uint8_t veces = 0x00;
        // Keep reading until receive 'num' of bytes or after 'veces' times.
        while ((rcv!=num) && (veces<0x02)) {
            pnt = &bufferRx[rcv];
            int aux_rcv = read(fd, pnt, num-rcv);
            // ROS_INFO("Ep %x, %x" ,veces, aux_rcv);
            rcv = rcv + aux_rcv;
            veces += 1;
        }
        // Check which of the conditions broke the 'while'
        if (veces >= 0x02) {
            usleep(100);
            tcflush(fd,TCIOFLUSH);
            return false;
        }else if (rcv == 0) {
            return false;
        }else {
            rx_complete = true;    
            return true;
        }
        
    }else{
        rx_complete = true;
        return true;    
    }
}

uint8_t MotorIO::ping(uint8_t slave_address) {
    //
    // Returns the address of the pinged slave OR 0 zero if no response to ping
    //
	// printf("Scanning for Arduino in '%s'...\r\n", i2cDevName);
    // ROS_INFO("--->Scanning for motor in '%s'...", portName);
    dummy += 1;
    // ROS_INFO("dummy = %x", dummy);
    bufferTx[2] = slave_address;
    bufferTx[3] = TX_LENGTH;
    bufferTx[4] = PING;
    bufferTx[5] = 0x00;
    bufferTx[6] = 0x00;
    bufferTx[7] = 0x00;
    bufferTx[8] = 0x00;
    uint8_t num_send = _crcTx(bufferTx, TX_LENGTH);
    
    if (write(fd, bufferTx, num_send) != num_send) {
    	ROS_INFO("NopEp! Couldn't send!!");
    }else {
    	rx_enabled = true;
    }
    // usleep(100); //required to make flush work, for some reason
    // tcflush(fd, TCIOFLUSH);

    if (rx_enabled == true) {
        if (read_response(7) == true) {
            // Check the CRC
            if (_crcRx(bufferRx) == true) {
                // Check the ID
                if (bufferRx[2] != 0) {
                    if ((bufferRx[2]!=slave_address) && (bufferRx[2]!=0)) {
                        ROS_WARN("Response from wrong motor!!");
                        return 0x07;
                    }else if (bufferRx[4]!=0) {
                        ROS_WARN("Arduino sent error: %x", bufferTx[4]);
                        return 0x06;
                    }else {
                        ROS_INFO("Found motor with ID: %x", bufferRx[2]);
                        return 0x00;
                    }
                }
            }else {
                ROS_WARN("Error in _crcRx");
            }

        }else {
            // ROS_ERROR("No response from %x !!", slave_address);
            return 0x01;
        }
        rx_enabled = false;
        rx_complete = false;
    }
    // usleep(100); //required to make flush work, for some reason
    // tcflush(fd,TCIOFLUSH);
}


uint8_t MotorIO::setDuty(uint8_t slave_address, uint8_t duty) {
    // ROS_INFO("Sending duty: %x", duty);

    bufferTx[2] = slave_address;
    bufferTx[3] = TX_LENGTH;
    bufferTx[4] = DUTY;
    bufferTx[5] = duty;
    bufferTx[6] = 0x00;
    bufferTx[7] = 0x00;
    bufferTx[8] = 0x00;
    uint8_t num_send = _crcTx(bufferTx, TX_LENGTH);

    if (write(fd, bufferTx, num_send) != num_send) {
        ROS_INFO("NopEp! Couldn't send!!");
    }else {
        rx_enabled = true;
    }
    if (rx_enabled == true) {
        if (read_response(7) == true) {
            // Check the CRC
            if (_crcRx(bufferRx) == true) {
                // Check the ID
                if (bufferRx[2] != 0) {
                    if ((bufferRx[2]!=slave_address) && (bufferRx[2]!=0)) {
                        ROS_WARN("Response from wrong motor!!");
                        return 0x07;
                    }else if (bufferRx[4]!=0) {
                        ROS_WARN("Arduino sent error: %x", bufferTx[4]);
                        return 0x06;
                    }else {
                        // ROS_INFO("Found motor with ID: %x", bufferRx[2]);
                        return 0x00;
                    }
                }
            }else {
                ROS_WARN("Error in _crcRx");
            }

        }else {
            ROS_ERROR("No response from %x !!", slave_address);
            return 0x01;
        }
        rx_enabled = false;
        rx_complete = false;
    }
    
}



uint8_t MotorIO::update_motors_list(uint8_t *lst) {
    int16_t idx = 0;
    ROS_INFO("Scanning for motors in '%s'...", portName);

    for (uint8_t addr=0x00;addr<0x13;addr++) {
        uint8_t e = ping(addr);
        if (e == 0x00) {
            lst[idx] = addr;
            idx += 1;
        }
    }
    return idx;
}

uint8_t MotorIO::get_position(uint8_t slave_address, int16_t &position) {
    ROS_INFO("Asking for position...");
    bufferTx[2] = slave_address;
    bufferTx[3] = TX_LENGTH;
    bufferTx[4] = GET_POS;
    bufferTx[5] = 0x00;
    bufferTx[6] = 0x00;
    bufferTx[7] = 0x00;
    bufferTx[8] = 0x00;
    uint8_t num_send = _crcTx(bufferTx, TX_LENGTH);

    if (write(fd, bufferTx, num_send)!= num_send) {
        ROS_INFO("Error in asking for position");
    }else {
        rx_enabled = true;
    }
    if (rx_enabled == true) {
        if (read_response(9) == true) {
            // Check the CRC
            if (_crcRx(bufferRx) == true) {
                // Check the ID
                if (bufferRx[2] != 0) {
                    if ((bufferRx[2]!=slave_address) && (bufferRx[2]!=0)) {
                        ROS_WARN("Response from wrong motor!!");
                        return 0x07;
                    }else if (bufferRx[4]!=0) {
                        ROS_WARN("Arduino sent error: %x", bufferTx[4]);
                        return 0x06;
                    }else {
                        int16_t *p = (int16_t *)&bufferRx[5];
                        position = *p;
                        ROS_INFO("Got position from motor ID: %x", bufferRx[2]);
                        return 0x00;
                    }
                }
            }else {
                ROS_WARN("Error in _crcRx");
            }

        }else {
            ROS_ERROR("No response from %x !!", slave_address);
            return 0x01;
        }
        rx_enabled = false;
        rx_complete = false;
    }
    // usleep(100); //required to make flush work, for some reason
    // tcflush(fd,TCIOFLUSH);
}


uint8_t MotorIO::get_velocity(uint8_t slave_address, int16_t &velocity) {
    ROS_INFO("Asking for velocity...");

    bufferTx[2] = slave_address;
    bufferTx[3] = TX_LENGTH;
    bufferTx[4] = GET_VEL;
    bufferTx[5] = 0x00;
    bufferTx[6] = 0x00;
    bufferTx[7] = 0x00;
    bufferTx[8] = 0x00;
    uint8_t num_send = _crcTx(bufferTx, TX_LENGTH);

    if (write(fd, bufferTx, num_send) != num_send) {
        ROS_INFO("NopEp! Couldn't send!!");
    }else {
        rx_enabled = true;
    }


    if (rx_enabled == true) {
        if (read_response(9) == true) {
            // Check the CRC
            if (_crcRx(bufferRx) == true) {
                // Check the ID
                if ((bufferRx[2]!=slave_address) && (bufferRx[2]!=0)) {
                    ROS_WARN("Response from wrong motor!!");
                    return 0x07;
                }else if (bufferRx[4]!=0) {
                    ROS_WARN("Arduino sent error: %x", bufferTx[4]);
                    return 0x06;
                }else {
                    int *p = (int *)&bufferRx[5];
                    velocity = *p;
                    ROS_INFO("Got velocity from: %x", bufferRx[2]);
                    return 0x00;
                }
            }else {
                ROS_WARN("Error in _crcRx");
            }

        }else {
            ROS_ERROR("No response from %x !!", slave_address);
            return 0x01;
        }
        rx_enabled = false;
        rx_complete = false;
    }

}



    


uint8_t MotorIO::feedback3(uint8_t slave_address, float &pos, float &vel, float &curr) {
    bufferTx[2] = slave_address;
    bufferTx[3] = TX_LENGTH;
    bufferTx[4] = FEEDBACK3;
    bufferTx[5] = 0x00;
    bufferTx[6] = 0x00;
    bufferTx[7] = 0x00;
    bufferTx[8] = 0x00;
    uint8_t num_send = _crcTx(bufferTx, TX_LENGTH);
    if (write(fd, bufferTx, num_send) != num_send) {
        ROS_INFO("Nop! Couldn't send!!");
    }else {
        rx_enabled = true;
    }
    usleep(600);
    if (rx_enabled == true) {
        // int a = read(fd, bufferRx, 19);
        // ROS_INFO("I read %i",a);
        // if (a != 19) {
        if (read(fd, bufferRx, 19)!=19){
            ROS_INFO("No response from '%x'!",slave_address);
        }else {
            uint8_t rx_error = bufferRx[4];
            if ((_crcRx(bufferRx)) && (rx_error == 0x00)) {
                p = (int16_t *)&bufferRx[5];
                v = (int16_t *)&bufferRx[7];
                c = (int16_t *)&bufferRx[9];
                pos = *p;
                vel = *v;
                curr = *c;

            }else {
                // TODO - Error debug
                ROS_INFO("Error in Rx buffer of motor with address %x", bufferRx[2]);
            }
        }
        rx_enabled = false;
    }
}


uint8_t MotorIO::set_gain(uint8_t slave_address, uint8_t gain, float val) {
    float *p = (float *)&val;
    bufferTx[2] = slave_address;
    bufferTx[3] = TX_LENGTH;
    if (gain == 'P') {
        bufferTx[4] = PGAIN;
    }else if (gain =='I') {
        bufferTx[4] = IGAIN;
    }else if (gain == 'D') {
        bufferTx[4] = DGAIN;
    }else {
        ROS_ERROR("Please choose one of the 'P/I/D' gain");
        return 0x01;
    }
    bufferTx[5] = *p;
    bufferTx[6] = *(p+1);
    bufferTx[7] = *(p+2);
    bufferTx[8] = *(p+3);
    uint8_t num_send = _crcTx(bufferTx, TX_LENGTH);
    
    if (write(fd, bufferTx, num_send) != num_send) {
        ROS_INFO("NopEp! Couldn't send!!");
    }else {
        rx_enabled = true;
    }

    if (rx_enabled == true) {
        if (read_response(7) == true) {
            // Check the CRC
            if (_crcRx(bufferRx) == true) {
                // Check the ID
                if (bufferRx[2] != 0) {
                    if ((bufferRx[2]!=slave_address) && (bufferRx[2]!=0)) {
                        ROS_WARN("Response from wrong motor!!");
                        return 0x07;
                    }else if (bufferRx[4]!=0) {
                        ROS_WARN("Arduino sent error: %x", bufferTx[4]);
                        return 0x06;
                    }else {
                        ROS_INFO("Gain '%c' successfully sent to motor with ID: %x", gain,  bufferRx[2]);
                        return 0x00;
                    }
                }
            }else {
                ROS_WARN("Error in _crcRx");
            }

        }else {
            ROS_ERROR("No response from %x !!", slave_address);
            return 0x01;
        }
        rx_enabled = false;
        rx_complete = false;
    }    

}


uint8_t MotorIO::goal_position(uint8_t slave_address, float pos) {
    bufferTx[2] = slave_address;
    bufferTx[3] = TX_LENGTH;
    bufferTx[4] = GOAL_POSITION;
    uint8_t *pnt = (uint8_t *)&pos;
    for (uint8_t i=0;i<4;i++) {
        bufferTx[5+i] = *(pnt+i);
    }
    uint8_t num_send = _crcTx(bufferTx, TX_LENGTH);
    // for (int i=0;i<num_send;i++) {
    //  ROS_INFO("bufferTx[%i] = %x", i, bufferTx[i]);
    // }
    if (write(fd, bufferTx, num_send) != num_send) {
        ROS_INFO("Nop! Couldn't send!!");
    }else {
        rx_enabled = true;
    }
    usleep(600);
    if (rx_enabled == true) {
        if (read(fd, bufferRx, 7)!=7){
            ROS_INFO("No response from '%x'!",slave_address);
        }else {
            uint8_t rx_error = bufferRx[4];
            if ((_crcRx(bufferRx)) && (rx_error == 0x00)) {
                // ida y vuelta succesfull
                return bufferRx[2];
            }else {
                // TODO - Error debug
                ROS_INFO("Error in Rx buffer of motor with address %x", bufferRx[2]);
                return bufferRx[2];
            }
        }
        rx_enabled = false;
    }
}

uint8_t MotorIO::goal_velocity(uint8_t slave_address, int16_t vel) {
    uint8_t *p = (uint8_t *)&vel;
    bufferTx[2] = slave_address;
    bufferTx[3] = TX_LENGTH;
    bufferTx[4] = SET_VEL;
    bufferTx[5] = *p;
    bufferTx[6] = *(p+1);
    bufferTx[7] = 0x00;
    bufferTx[8] = 0x00;
    uint8_t num_send = _crcTx(bufferTx, TX_LENGTH);
    
    if (write(fd, bufferTx, num_send) != num_send) {
        ROS_INFO("NopEp! Couldn't send!!");
    }else {
        rx_enabled = true;
    }

    if (rx_enabled == true) {
        if (read_response(7) == true) {
            // Check the CRC
            if (_crcRx(bufferRx) == true) {
                // Check the ID
                if (bufferRx[2] != 0) {
                    if ((bufferRx[2]!=slave_address) && (bufferRx[2]!=0)) {
                        ROS_WARN("Response from wrong motor!!");
                        return 0x07;
                    }else if (bufferRx[4]!=0) {
                        ROS_WARN("Arduino sent error: %x", bufferTx[4]);
                        return 0x06;
                    }else {
                        ROS_INFO("Velocity reference successfully sent to motor with ID: %x", bufferRx[2]);
                        return 0x00;
                    }
                }
            }else {
                ROS_WARN("Error in _crcRx");
            }

        }else {
            ROS_ERROR("No response from %x !!", slave_address);
            return 0x01;
        }
        rx_enabled = false;
        rx_complete = false;
    }    

}












void MotorIO::generate_CRC16() {
    /* Calculates the CRC16 Look-up table */

    const ushort generator = 0x1021;
    for (int divident = 0; divident < 256; divident++) /* iterate over all possible input byte values 0 - 255 */
    {
        unsigned short curByte = (unsigned short)(divident << 8); /* move divident byte into MSB of 16Bit CRC */ 
        for (unsigned char bit = 0; bit < 8; bit++)
        {
            if ((curByte & 0x8000) != 0)
            {
                curByte <<= 1;
                curByte ^= generator;
            }
            else
            {
                curByte <<= 1;
            }
        }
        crctable16[divident] = curByte;
    }
}


bool MotorIO::_crcRx(uint8_t *buf) {
    /* Checks the incoming data if they 'agree' with the CRC 
        Returns 'true' if there was no error in the data transmission, 
        and 'false' otherwise.
    */

    uint8_t len = buf[3];
    int16_t crc = 0;
    int16_t *p = (int16_t *)&buf[3+len];  // points to RX_CRC

    // Check incoming CRC
    for (uint8_t i=0; i<len; i++) 
    {
        uint8_t pos = (uint8_t)( (crc >> 8) ^ buf[i+3]); // equal: ((crc ^ (b << 8)) >> 8)
        // Shift out the MSB used for division per lookuptable and XOR with the remainder
        crc = (int16_t)((crc << 8) ^ (int16_t)(crctable16[pos]));

    }
    // ROS_INFO("CRC is: %x", crc);
    // ROS_INFO("CRC_pointer is: %x", *p);
    if (*p == crc) {
        return true;
    }else {
        return false;
    }
}


uint8_t MotorIO::_crcTx(uint8_t *buf, uint8_t len) {
    /* Calculates the CRC16 of the buffer we send, and adds the
        two CRC bytes at the end of the given buffer.
    */

    int16_t crc = 0;
    uint8_t *p = (uint8_t *)&crc;
    // calculate crc
    for (uint8_t i=0; i<len; i++) {
        uint8_t pos = (uint8_t)( (crc >> 8) ^ buf[i+3]);
        crc = (int16_t)((crc << 8) ^ (int16_t)(crctable16[pos]));
    }
    buf[3+len] = *p;
    buf[4+len] = *(p+1);
    return 5+len;
}


