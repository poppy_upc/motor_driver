#include <stdint.h>
#include <typeinfo>
#include <time.h>
#include <ros/ros.h>
#include <ros/package.h>
#include <ros/console.h>
#include <sstream>
#include <signal.h>
#include <fstream>

#include <motor_driver/serial_proxy.h>
// Messages
#include <motor_driver/Feedback.h>
#include <motor_driver/Velocity.h>
#include <motor_driver/Goals.h>
#include <motor_driver/Gains.h>
#include <motor_driver/Full_feedback.h>


uint8_t motor1, motor2, motor3;
const char *leg1 =  "/dev/ttyS2";
int16_t velocity;
int16_t position; /**< Position of the motor */
float pRad;
int16_t vel_reference = 0;
int16_t pos_reference;
uint8_t duty = 10;
float gain = 0;
uint8_t let;
bool gain_update = false;
uint8_t motors_list[12];
uint8_t nMotors;


// MotorIO com(leg1, "Left leg");
MotorIO com;


void mySigintHandler(int sig) {
  // Closes the serial port after 'Ctrl-C' was pressed
	com.stop();
	ROS_INFO("Motor_driver is closed!");
	ros::shutdown();
}


void callback_setGoal(const motor_driver::Goals::ConstPtr& msg)
{
  // ROS_INFO("ID of the motor you want to command is %i",msg->id);
  // ROS_INFO("Goal Position is %i",msg->goalPosition);
  // ROS_INFO("Goal Velocity is %i",msg->goalSpeed);
  // com.goal_velocities[msg->id] = msg->goalSpeed;
  // com.goal_positions[msg->id] = msg->goalPosition;
	pos_reference = msg->goal_position;
	vel_reference = msg->goal_velocity;
}

void callback_setGain(const motor_driver::Gains::ConstPtr& msg)
{
  // ROS_INFO("ID of the motor you want to command is %i",msg->id);
  // ROS_INFO("Goal Position is %i",msg->goalPosition);
  // ROS_INFO("Goal Velocity is %i",msg->goalSpeed);
  // com.goal_velocities[msg->id] = msg->goalSpeed;
  // com.goal_positions[msg->id] = msg->goalPosition;
	// pos_reference = msg->goal_position;
	// vel_reference = msg->goal_velocity;
	gain = msg->gain;
	let = msg->let;
	gain_update = true;
}


int main(int argc, char **argv) {

	ros::init (argc, argv, "serial_test");
	ros::NodeHandle nh;
	signal(SIGINT, mySigintHandler);  // to handle ctrl-c

	std::ofstream myFile;

	com.start();
	usleep(1000000);
	// ROS_INFO("Wait!");
	// ----------------------------------- Publishers ----------------------------------------//
	ros::Publisher motor1_pub = nh.advertise<motor_driver::Feedback>("motor_driver/motor1_feedback", 1000);
	ros::Publisher motor1_vel = nh.advertise<motor_driver::Velocity>("motor_driver/motor1_velocity", 1000);
	ros::Publisher motor_feedback = nh.advertise<motor_driver::Full_feedback>("motor_driver/motor_feedback",100);
	motor_driver::Feedback motor1_feed;
	motor_driver::Velocity motor1_v;
	motor_driver::Full_feedback ffb;	//FullFeedBack (ffb)

	// ------------------------------------ Subsricers ---------------------------------------//
	ros::Subscriber motor1_vref = nh.subscribe<motor_driver::Goals>("motor_driver/motor1_setGoal", 1000, &callback_setGoal);
	ros::Subscriber motor1_Gains = nh.subscribe<motor_driver::Gains>("motor_driver/motor1_Gains",1000, &callback_setGain);


	// motor1 = com.ping(0x01);
	// motor1 = com.ping(0x02);
	// motor1 = com.ping(0x03);
	// motor1 = com.ping(0x04);
	// motor1 = com.ping(0x05);
	// motor1 = com.ping(0x06);

	nMotors = com.update_motors_list(motors_list);
	ROS_INFO("Found '%x' motors", nMotors);
	// if (motor1 == 0x00) {
	// 	motor1 = com.get_position(0x04, position);
	// 	ROS_INFO("Position: %i", position);
	// }

	// usleep(1000000);
	// motor1 = com.get_velocity(0x04, velocity);
	// motor1 = com.setDuty(0x04, 0x00);
	// motor1 = com.ping(0x14);
	// motor2 = com.ping(0x15);
	// motor3 = com.ping(0x16);
	// ROS_INFO("Motor is %x", motor1);
	// ROS_INFO("Motor is %x", motor2);

	// com.goal_position(motor1, 170.55);
	// com.feedback3(motor1, com.position, com.velocity, com.current);
	// // com.feedback3(motor1, com.position, com.velocity, com.current);
	// ROS_INFO("Position is %f", com.position);
	// ROS_INFO("Velocity is %f", com.velocity);
	// ROS_INFO("Current is %f", com.current);
	// com.stop();
	// uint8_t total_motors = com.update_motors_list(com.motors_left);
	int16_t flag = 0;
	int16_t duty = 15;

	ros::Rate loop_rate(70);
	while(ros::ok())  {

		for (uint8_t i=0; i<nMotors; i++) {
			motor1 = com.get_position(motors_list[i],position);
			pRad = position*6.2932 / 4096;
			motor1 = com.get_velocity(motors_list[i],velocity);
			ffb.position[i] = pRad;
			ffb.velocity[i] = velocity;
		}
		motor_feedback.publish(ffb);

		/*-----------------------------------------------*/
		// motor1 = com.get_position(0x04, position);
		// // ROS_INFO("Position: %i", position);
		// motor1 = com.get_velocity(0x04, velocity);
		// // ROS_INFO("Velocity: %i", velocity);
		// motor1_feed.position = position;
		// motor1_feed.velocity = velocity;
		// motor1_pub.publish(motor1_feed);

		// motor1_v.velocity = velocity;
		// motor1_v.reference = vel_reference;
		// motor1_vel.publish(motor1_v);

		// com.goal_velocity(0x04, 30);
		/*-----------------------------------------------*/






		// if (gain_update == true) {
		// 	gain_update = false;
		// 	com.set_gain(0x04, let, gain);
		// }
		/*-----------------------------------------------*/
		// Data collection------------------------------------------
		// if (flag == 0) {
		// 	com.setDuty(0x04, 0);
		// 	flag = 1;	
		// }else if (flag == 1) {
		// 	int error = com.setDuty(0x04, duty);
		// 	ROS_INFO("Error: %i", error);
		// 	flag = 2;
		// }else if (flag == 2) {
		// 	flag = 3;
		// }else if (flag == 3) {
		// 	flag = 4;
		// }else if (flag == 4) {
		// 	com.get_velocity(0x04, velocity);
		// 	usleep(1000); // wait 1 ms
		// 	// write to file
		// 	myFile.open("vel_lin.txt", std::fstream::out | std::fstream::app);
		// 	// myFile << duty << ", "<< velocity << ", " << (int)com.duty;
		// 	myFile << duty << ", " << velocity;
		// 	myFile << "\n";
		// 	myFile.close();
		// 	// ROS_INFO("qd*, qd, duty: %i,  %f, %i",velocity, com.velocity, (int)com.duty);
		// 	ROS_INFO("'Duty sent, 'velocity' received': %i, %i", duty, velocity);
		// 	// -------------
		// 	duty += 1;
		// 	flag = 0;
			
		// }

		// if (duty > 28) {
		// 	com.setDuty(0x04, 0);
		// 	flag = 5;
		// }
		/*-----------------------------------------------*/
		// END of data collection------------------------------------------
		// if (flag == 0) {
		// 	motor1 = com.setDuty(0x04, 20);
		// 	flag = 1;
		// }else {
		// 	motor1 = com.setDuty(0x04, 40);
		// 	flag = 0;
		// }



		// motor1 = com.get_velocity(0x04, velocity);
		// uint8_t mpla = com.get_velocity(0x04, velocity);
		// ROS_INFO("Velocity_received: %i", velocity);
		// motor1 = com.ping(0x01);
		// motor1 = com.ping(0x02);
		// motor1 = com.ping(0x03);
		// com.feedback3(motor1, com.position, com.velocity, com.current);
		// ROS_INFO("Position is %f", com.position);
		// ROS_INFO("Velocity is %f", com.velocity);
		// ROS_INFO("Current is %f", com.current);
		// if (com.position < 180) {
		// 	ROS_ERROR("Ep Ep");
		// }


		ros::spinOnce();
    	loop_rate.sleep();
	}
}